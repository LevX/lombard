import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {User} from "../_models";
import {AuthenticationService} from "../_services/authentication.service";

@Component({
  selector: 'app-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.css']
})
export class LeftNavComponent implements OnInit {
  new_report_color: string;
  view_report_color: string;
  stats_color: string;
  users_color: string;
  branches_color: string;
  excel_color: string;
  catalog_color: string;
  user: User;

  selected_color = 'primary';


  constructor(private router: Router, private authServise: AuthenticationService) {
    console.log("logging in");
    authServise.currentUser.pipe(filter(u => u !== null)).subscribe(u => this.user = u);
    this.switchButtons();
    this.router.events.subscribe(
      (event: any) => {
        if (event instanceof NavigationEnd) {
          this.switchButtons();
        }
      }
    );
  }

  switchButtons() {
    console.log('this.router.url', this.router.url);
    this.new_report_color = '';
    this.view_report_color = '';
    this.stats_color = '';
    this.users_color = '';
    this.branches_color = '';
    this.excel_color = '';
    this.catalog_color = '';
    switch (this.router.url) {
      case '/':
      case '/newReport':
        this.new_report_color = this.selected_color;
        break;
      case '/viewReport':
        this.view_report_color = this.selected_color;
        break;
      case '/stats':
        this.stats_color = this.selected_color;
        break;
      case '/users':
        this.users_color = this.selected_color;
        break;
      case '/branches':
        this.branches_color = this.selected_color;
        break;
      case '/excel':
        this.excel_color = this.selected_color;
        break;
      case '/catalog':
        this.catalog_color = this.selected_color;
        break;
    }
  }

  ngOnInit() {
  }

  logout() {
    this.authServise.logout();
  }
}
