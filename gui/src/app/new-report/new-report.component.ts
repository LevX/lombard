import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {Branch, User} from "../_models";
import {BranchService} from "../_services/branch.service";
import {ReportService} from "../_services/report.service";
import {Report} from "../_models/report";
import {Expense} from "../_models/expense";
import {DynamicsService} from "../_services/dynamics.service";
import {Dynamics} from "../_models/dynamics";
import {isUndefined} from 'util';
import {UserService} from "../_services/user.service";
import {ModalNotificationService} from "../modalNotifications/modal.notification.service";
import {Tools} from "../_helpers/tools";
import {DateFormats} from "../_helpers/dateFormats";

@Component({
  selector: 'app-new-report',
  templateUrl: './new-report.component.html',
  styleUrls: ['./new-report.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DateFormats}
  ]
})
export class NewReportComponent implements OnInit {

  users: User[];
  report: Report;
  dynamics: Dynamics = new Dynamics();

  BRANCH_DATA: Branch[];
  branches = this.BRANCH_DATA;


  loading = false;
  saveDisabled = true;
  saveText = "Сохранить";
  disableExpenseSave = true;
  formValue = "";
  currentUser;
  auctionAmount;
  version = 1;
  //=====
  formChanged = false;
  loadingReport = false;
  loadingDynamics = false;
  saving = false;
  //=====
  saveDisabledInfo = "";

  saveReportForm: FormGroup = new FormGroup({
    id: new FormControl('0'),
    branch: new FormControl('', Validators.required),
    date: new FormControl(new Date(), Validators.required),
    loanersPawned: new FormControl("0"),
    loanersBought: new FormControl("0"),
    loanersAsset: new FormControl("0"),
    // Корзина
    loanedRub: new FormControl('0'),
    repayedRub: new FormControl('0'),
    percentRecieved: new FormControl('0'),
    volume: new FormControl('0'),
    // Золото
    goldBought: new FormControl('0.00'),
    goldSold: new FormControl('0.00'),
    goldBalance: new FormControl('0.00'),
    // Серебро
    silverBought: new FormControl('0.00'),
    silverSold: new FormControl('0.00'),
    silverBalance: new FormControl('0.00'),
    // Бриллианты
    diamondBought: new FormControl('0.00'),
    diamondSold: new FormControl('0.00'),
    diamondBalance: new FormControl('0.00'),
    // Вещи
    goodsBought: new FormControl('0'),
    goodsSold: new FormControl('0'),
    goodsBalance: new FormControl('0'),
    // Касса
    cashboxMorning: new FormControl('0'),
    cashboxEvening: new FormControl(''),
    // Торги
    tradesActive: new FormControl('0'),
    goldTradeSum: new FormControl('0'),
    goldTradeWeight: new FormControl('0.00'),
    silverTradeSum: new FormControl('0'),
    silverTradeWeight: new FormControl('0.00'),
    diamondsTradeWeight: new FormControl('0.00'),
    goodsTradeSum: new FormControl('0'),
    tradeSum: new FormControl("0"),
    expenseName: new FormControl(''),
    expenseSum: new FormControl('')
  });

  constructor(private userService: UserService,
              private branchService: BranchService,
              private reportService: ReportService,
              private dynamicsService: DynamicsService,
              private notificationDialog: ModalNotificationService) {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    if (this.currentUser.roles.indexOf("admin") >= 0) {
      this.branchService.getAll().subscribe(branches => {
        this.branches = branches;
      });
    } else {
      this.branchService.getBranchesByIDs(this.currentUser.branches).subscribe(branches => {
        this.branches = branches;
      });
    }
    this.userService.getAll().subscribe(users => {
      this.users = users;
    });
    this.saveReportForm.valueChanges.subscribe(
      result => {
        this.formChanged = true;
      }
    )
  }

  public isSaveDisabled(): boolean {
    this.saveDisabledInfo = "";
    if (!isUndefined(this.auctionAmount) && this.auctionAmount != 0 && this.currentUser.roles.indexOf("admin") < 0) {
      // console.log("save disabled by auctionAmount[" + this.auctionAmount + "]");
      this.saveText = "Сохранение при внесённых данных по торгам невозможно.";
      this.saveDisabled = true;
      return this.saveDisabled;
    }
    if (this.f.cashboxEvening.value == '') {
      this.saveText = "Сохранение без указания Остатка кассы на конец дня невозможно.";
      // console.log("save disabled by empty cashboxEvening");
      this.saveDisabled = true;
      return this.saveDisabled;
    }
    if (this.f.branch.value == "" || this.f.date.value == "") {
      this.saveDisabled = true;
      this.saveText = "Сохранение невозможно так как поля Подразделение или Дата пусты.";
      // console.log("save disabled by empty branch or date");
      return this.saveDisabled;
    }
    if (this.loadingReport) {
      this.saveText = "Сохранение при незавершенной загрузке данных невозможно.";
      // console.log("save disabled by loading report");
      this.saveDisabled = true;
      return this.saveDisabled;
    }
    if (this.loadingDynamics) {
      // console.log("save disabled by loading dynamics");
      this.saveText = "Сохранение при незавершенной загрузке данных невозможно.";
      this.saveDisabled = true;
      return this.saveDisabled;
    }
    if (this.formValue == this.saveReportForm.value.toString()) {
      // console.log("save disabled by unchanged form");
      this.saveText = "Сохранение без изменения данных невозможно.";
      this.saveDisabled = true;
      return this.saveDisabled;
    }
    if (this.saving) {
      // console.log("save disabled by saving");
      this.saveText = "Повторное сохранение при сохранении невозможно.";
      this.saveDisabled = true;
      return this.saveDisabled;
    }
    if (this.report != null && this.report.user != undefined && this.currentUser.roles.indexOf("admin") == -1) {
      if (this.report.user != this.currentUser._id) {
        this.saveText = "Отчет уже сохранён пользователем " + this.getUserByID(this.report.user) + " и не может быть изменён вами";
        this.saveDisabled = true;
        return this.saveDisabled;
      }
      if ((new Date().getTime() - new Date(this.report.date).getTime()) > 48 * 60 * 60 * 1000) {
        this.saveText = "Нельзя редактировать отчет, сохраненный более 2х дней назад";
        this.saveDisabled = true;
        return this.saveDisabled;
      }
    }
    this.saveText = "Сохранить";
    this.saveDisabled = false;
    return this.saveDisabled;
  }

  getUserByID(id: string): string {
    if (this.users != null) {
      for (let u of this.users) {
        if (u._id == id) {
          return u.name;
        }
      }
    }
    return "[deleted]";
  }

  get f() {
    return this.saveReportForm.controls;
  }

  ngOnInit() {

  }

  public changeDynamics(): void {
    console.log("save state in changeDynamics:" + this.saveDisabled);
    let currentState = this.saveDisabled;
    console.log(this.dynamics);
    if (!isUndefined(this.dynamics.loanersAsset)) {
      this.f.loanersAsset.patchValue(this.transformInput((Tools.strToNum(this.f.loanersPawned.value) - Tools.strToNum(this.f.loanersBought.value) + this.dynamics.loanersAsset - Tools.strToNum(this.f.tradesActive.value)).toString()));
    }
    if (!isUndefined(this.dynamics.volume)) {
      this.f.volume.patchValue(this.transformInput((Tools.strToNum(this.f.loanedRub.value) - Tools.strToNum(this.f.repayedRub.value) - Tools.strToNum(this.f.goldTradeSum.value)
        - Tools.strToNum(this.f.silverTradeSum.value) - Tools.strToNum(this.f.goodsTradeSum.value) + this.dynamics.volume).toString()));
    }
    if (!isUndefined(this.dynamics.goldBalance)) {
      this.f.goldBalance.patchValue(Tools.formatDecimals((Tools.strToNum(this.f.goldBought.value) - Tools.strToNum(this.f.goldSold.value) + this.dynamics.goldBalance - Tools.strToNum(this.f.goldTradeWeight.value)).toString()));
    }
    if (!isUndefined(this.dynamics.silverBalance)) {
      this.f.silverBalance.patchValue(Tools.formatDecimals((Tools.strToNum(this.f.silverBought.value) - Tools.strToNum(this.f.silverSold.value) + this.dynamics.silverBalance - Tools.strToNum(this.f.silverTradeWeight.value)).toString()));
    }
    if (!isUndefined(this.dynamics.diamondBalance)) {
      this.f.diamondBalance.patchValue(Tools.formatDecimals((Tools.strToNum(this.f.diamondBought.value) - Tools.strToNum(this.f.diamondSold.value) + this.dynamics.diamondBalance - Tools.strToNum(this.f.diamondsTradeWeight.value)).toString()));
    }
    if (!isUndefined(this.dynamics.goodsBalance)) {
      this.f.goodsBalance.patchValue(this.transformInput((Tools.strToNum(this.f.goodsBought.value) - Tools.strToNum(this.f.goodsSold.value) + this.dynamics.goodsBalance - Tools.strToNum(this.f.goodsTradeSum.value)).toString()));
    }
    if (!isUndefined(this.dynamics.cashboxEvening)) {
      this.f.cashboxMorning.patchValue(this.transformInput(this.dynamics.cashboxEvening.toString()));
    }
    this.f.tradeSum.patchValue(this.transformInput((Tools.strToNum(this.f.goldTradeSum.value) + Tools.strToNum(this.f.silverTradeSum.value) + Tools.strToNum(this.f.goodsTradeSum.value)).toString()));
    if (currentState == true) {
      //this.formValue = this.saveReportForm.value.toString();
      this.saveDisabled = currentState;
    }
  }

  valuechange(): void {
    if (this.f.branch.value == "" || this.f.date.value == "") {
      return;
    }
    console.log("update required");
    let foundReport: Report;
    this.loadingReport = true;
    this.loadingDynamics = true;

    this.reportService.getReportByDateAndBranch(this.f.date.value, this.f.branch.value).subscribe(rpt => {
        console.log(rpt);
        if (rpt.length > 0) {
          let lr = rpt[0];
          console.log(lr);
          this.f.id.patchValue(lr._id);
          this.f.loanersPawned.patchValue(Tools.formatNum(lr.loanersPawned.toString()));
          this.f.loanersBought.patchValue(Tools.formatNum(lr.loanersBought.toString()));
          this.f.loanedRub.patchValue(Tools.formatNum(lr.loanedRub.toString()));
          this.f.repayedRub.patchValue(Tools.formatNum(lr.repayedRub.toString()));
          this.f.percentRecieved.patchValue(Tools.formatNum(lr.percentRecieved.toString()));
          // Золото
          this.f.goldBought.patchValue(Tools.formatNum(lr.goldBought.toString()));
          this.f.goldSold.patchValue(Tools.formatNum(lr.goldSold.toString()));
          // Серебро
          this.f.silverBought.patchValue(Tools.formatNum(lr.silverBought.toString()));
          this.f.silverSold.patchValue(Tools.formatNum(lr.silverSold.toString()));
          // Бриллианты
          this.f.diamondBought.patchValue(Tools.formatNum(lr.diamondBought.toString()));
          this.f.diamondSold.patchValue(Tools.formatNum(lr.diamondSold.toString()));
          // Вещи
          this.f.goodsBought.patchValue(Tools.formatNum(lr.goodsBought.toString()));
          this.f.goodsSold.patchValue(Tools.formatNum(lr.goodsSold.toString()));
          // Касса
          this.f.cashboxEvening.patchValue(Tools.formatNum(lr.cashboxEvening.toString()));
          // Торги
          this.f.tradesActive.patchValue(Tools.formatNum(lr.tradesActive.toString()));
          this.f.goldTradeSum.patchValue(Tools.formatNum(lr.goldTradeSum.toString()));
          this.f.goldTradeWeight.patchValue(Tools.formatNum(lr.goldTradeWeight.toString()));
          this.f.silverTradeSum.patchValue(Tools.formatNum(lr.silverTradeSum.toString()));
          this.f.silverTradeWeight.patchValue(Tools.formatNum(lr.silverTradeWeight.toString()));
          this.f.diamondsTradeWeight.patchValue(Tools.formatNum(lr.diamondsTradeWeight.toString()));
          this.f.goodsTradeSum.patchValue(Tools.formatNum(lr.goodsTradeSum.toString()));
          this.f.expenseName.patchValue('');
          this.f.expenseSum.patchValue('');
          this.report = lr;
          this.changeDynamics();
          this.auctionAmount = lr.auctionAmount;
          this.version = lr.version;
          console.log(lr.version);
        } else {
          this.resetForm();
        }
        if (!this.saveDisabled) {
          console.log("remembering form");
          this.formValue = this.saveReportForm.value.toString();
        }
        this.loadingReport = false;
      }, error => {
        console.log(error);
        this.loadingReport = false;
      }
    );
    this.dynamicsService.getDynamics(this.f.branch.value, this.f.date.value).subscribe(dyn => {
      this.dynamics = dyn;
      this.changeDynamics();
      this.loadingDynamics = false;
    });
  }

  addExpense() {
    console.log("name:" + this.f.expenseName.value);
    console.log("sum:" + this.f.expenseSum.value)
    if (this.report == null) {
      this.report = new Report();
    }
    if (this.report.expenses == null) {
      this.report.expenses = [];
    }
    let expense = new Expense();
    expense.id = new Date().getTime().toString();
    expense.name = this.f.expenseName.value;
    expense.sum = Tools.getValue(this.f.expenseSum.value);
    console.log(expense);
    this.report.expenses.push(expense);
    this.f.expenseName.patchValue('');
    this.f.expenseSum.patchValue('');
    if (this.f.branch.value != '' && this.f.date.value != '') {
      console.log("trying to enable save");
      this.saveDisabled = false;
    }
  }

  removeExpense(id: string) {
    console.log("exId:" + id);
    if (this.report != null && this.report.expenses != null) {
      for (let i = 0; i < this.report.expenses.length; i++) {
        if (this.report.expenses[i].id == id) {
          this.report.expenses.splice(i, 1);
        }
      }
    }
    if (this.f.branch.value != '' && this.f.date.value != '') {
      console.log("trying to enable save");
      this.saveDisabled = false;
    }
  }

  resetForm() {
    console.log("resetting form");
    this.saveDisabled = true;
    this.auctionAmount = undefined;
    this.report = new Report();
    this.f.id.patchValue("");
    this.f.loanersPawned.patchValue("0");
    this.f.loanersBought.patchValue("0");
    this.f.loanersAsset.patchValue("0");
    this.f.loanedRub.patchValue("0");
    this.f.repayedRub.patchValue("0");
    this.f.percentRecieved.patchValue("0");
    this.f.volume.patchValue("0");
    // Золото
    this.f.goldBought.patchValue("0");
    this.f.goldSold.patchValue("0");
    this.f.goldBalance.patchValue("0");
    // Серебро
    this.f.silverBought.patchValue("0");
    this.f.silverSold.patchValue("0");
    this.f.silverBalance.patchValue("0");
    // Бриллианты
    this.f.diamondBought.patchValue("0");
    this.f.diamondSold.patchValue("0");
    this.f.diamondBalance.patchValue("0");
    // Вещи
    this.f.goodsBought.patchValue("0");
    this.f.goodsSold.patchValue("0");
    this.f.goodsBalance.patchValue("0");
    // Касса
    this.f.cashboxMorning.patchValue("0");
    this.f.cashboxEvening.patchValue("");
    // Торги
    this.f.tradesActive.patchValue("0");
    this.f.goldTradeSum.patchValue("0");
    this.f.goldTradeWeight.patchValue("0");
    this.f.silverTradeSum.patchValue("0");
    this.f.silverTradeWeight.patchValue("0");
    this.f.diamondsTradeWeight.patchValue("0");
    this.f.goodsTradeSum.patchValue("0");
    this.f.tradeSum.patchValue("0");
    this.f.expenseName.patchValue('');
    this.f.expenseSum.patchValue('');
  }

  expensesChanged() {
    if (this.f.expenseName.value.trim() == '') {
      this.disableExpenseSave = true;
      return;
    }
    if (isNaN(parseFloat(this.f.expenseSum.value)) || Number(this.f.expenseSum.value) == 0) {
      this.disableExpenseSave = true;
      return;
    }
    this.disableExpenseSave = false;
  }

  saveReport(): void {
    if (!
      this.saveDisabled && !this.saving
    ) {
      this.saving = true;
      this.loading = true;
      this.saveReportForm.disable();
      this.saveDisabled = true;
      this.reportService.create(
        this.f.id.value,
        this.f.branch.value,
        this.f.date.value,

        Tools.getValue(this.f.loanersPawned.value),
        Tools.getValue(this.f.loanersBought.value),

        Tools.getValue(this.f.loanedRub.value),
        Tools.getValue(this.f.repayedRub.value),
        Tools.getValue(this.f.percentRecieved.value),
        // Золото
        Tools.getValue(this.f.goldBought.value),
        Tools.getValue(this.f.goldSold.value),
        // Серебро
        Tools.getValue(this.f.silverBought.value),
        Tools.getValue(this.f.silverSold.value),
        // Бриллианты
        Tools.getValue(this.f.diamondBought.value),
        Tools.getValue(this.f.diamondSold.value),
        // Вещи
        Tools.getValue(this.f.goodsBought.value),
        Tools.getValue(this.f.goodsSold.value),
        // Kassa
        Tools.getValue(this.f.cashboxEvening.value),
        // Торги
        Tools.getValue(this.f.tradesActive.value),
        Tools.getValue(this.f.goldTradeSum.value),
        Tools.getValue(this.f.goldTradeWeight.value),
        Tools.getValue(this.f.silverTradeSum.value),
        Tools.getValue(this.f.silverTradeWeight.value),
        Tools.getValue(this.f.diamondsTradeWeight.value),
        Tools.getValue(this.f.goodsTradeSum.value),
        this.report.expenses,
        this.auctionAmount,
        this.version
      ).subscribe(data => {
          console.log(data);
          this.setStates();
          this.saving = false;
          this.valuechange()
        },
        error => {
          if (error == "OK") {
            this.setStates();
            this.valuechange()
            this.saving = false;
          } else {
            this.notificationDialog.open("Отчёт за это число уже сохранён.", true);
            this.saveReportForm.enable();
            this.loading = false;
            this.saveDisabled = true;
            this.saving = false;
            this.valuechange();
          }
        });
    }
  }

  setStates(): void {
    this.saveReportForm.enable();
    this.loading = false;
    this.saveDisabled = true;
    this.notificationDialog.open("Спасибо, отчет принят", true);
  }

  transformInput(value: string): string {
    return parseFloat(value).toLocaleString("ru-RU");
  }

  public keyDown(event, allowDecimals: boolean) {
    Tools.allowNumbers(event, allowDecimals);
  }

  public keyUp(event, allowEmpty) {
    Tools.keyUpProcessor(event, allowEmpty);
  }

  onBlur(event) {
    event.target.value = Tools.formatDecimals(event.target.value);
  }

  onBlurFixed(event, allowEmpty) {
    if (allowEmpty && event.target.value == '') {
      return;
    }
    console.log("onBlurFixed", event, allowEmpty);
    event.target.value = Tools.strToNum(event.target.value).toLocaleString("ru-RU");
  }
}
