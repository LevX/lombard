import * as _moment from 'moment';

export class Tools {

  //get timestamp in locale with time set
  public static getTimestamp(date: string, hour: number, min: number, sec: number, ms: number): number {
    let d = new Date(date).setHours(hour, min, sec, ms);
    return new Date(d).getTime() - (new Date().getTimezoneOffset() * 60 * 1000);
  }

  //add thousand separators
  public static formatNum(value: string): string {
    if (value.trim() == '') {
      return "";
    }
    let normalized = value.replace(/,/g, '.');
    let sp = normalized.split('.');
    let returnValue = parseFloat(sp[0].replace(/\s/g, '')).toLocaleString("ru-RU");
    if (isNaN(parseFloat(returnValue.replace(/,/g, '.').replace(' ', '')))) {
      return "0";
    }
    if (sp.length > 1) {
      return returnValue + ',' + sp[1];
    } else {
      return returnValue;
    }
  }

  public static allowNumbers(event: KeyboardEvent, allowDecimals: boolean) {
    switch (event.key) {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
      case '-':

      case 'Home':
      case "End":
      case "ArrowLeft":
      case "ArrowRight":
      case "Tab":
      case "Enter":
      case "Delete":
      case "Backspace":
        return true;
      case '.':
      case ',':
        return allowDecimals;
      default:
        return false;
    }
  }

  //format on unfocus
  public static keyUpProcessor(event, allowEmpty) {
    if (allowEmpty && event.target.value == '') {
      return;
    }
    event.target.value = Tools.formatNum(event.target.value);
  }

  public static strToNum(value: string): number {
    if (value == "") {
      return 0;
    }
    return parseFloat(Tools.getValue(value.toString()));
  }

  public static getValue(value: string): string {
    if (value == "") {
      return "0";
    }
    return value.replace(/\s/g, '').replace(/,/g, '.');
  }

  public static updateLocale() {
    var moment = _moment;
    moment.updateLocale('en', {
      months: [
        "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля",
        "Августа", "Сентября", "Октября", "Ноября", "Декабря"
      ],
      monthsShort: [
        "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль",
        "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
      ],
      week: {
        dow: 1,
        doy: 1
      }
    });
    return moment;
  }

  public static formatDecimals(value: string): string {
    value = parseFloat(Tools.strToNum(value).toFixed(2)).toLocaleString("ru-RU");
    if (value.indexOf(",") > 0) {
      value = value + "00";
    } else {
      value = value + ",00";
    }
    return value.substring(0, value.indexOf(",") + 3);
  }
}
