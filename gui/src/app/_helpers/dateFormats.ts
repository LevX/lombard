export const DateFormats = {
  parse: {
    dateInput: 'DD',
  },
  display: {
    dateInput: 'DD MMMM YYYY',
    monthYearLabel: 'DD MMMM YYYY',
    dateA11yLabel: 'DD MMMM YYYY',
    monthYearA11yLabel: 'DD MMMM YYYY',
  },
};
