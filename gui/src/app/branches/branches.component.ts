import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {BranchService} from '../_services/branch.service';
import {Branch} from '../_models';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalNotificationService} from "../modalNotifications/modal.notification.service";
import {LegalParty} from "../_models/legalParty";
import {LegalPartyService} from "../_services/legalParty_service";

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.css']
})
export class BranchesComponent implements OnInit {

  branches: Branch[];
  legalParties: LegalParty[];

  constructor(
    private dialog: MatDialog,
    private branchService: BranchService,
    private legalPartyService: LegalPartyService) {
    this.refresh();

  }

  ngOnInit() {
  }

  refresh() {
    this.legalPartyService.getAll().subscribe(data => this.legalParties = data);
    this.branchService.getAllWithUsers().subscribe(data => this.branches = data);
  }

  getLegalPartyName(legalPartyId: string): string {
    for (let lp of this.legalParties) {
      if (lp._id == legalPartyId) {
        return lp.name;
      }
    }
    return "";
  }

  openAddDialog(): void {
    const dialogRef = this.dialog.open(EditBranchesComponent, {
      width: '700px',
      data: {
        new: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh();
    });
  }

  openEditDialog(id: string): void {
    const dialogRef = this.dialog.open(EditBranchesComponent, {
      width: '700px',
      data: {
        id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh();
    });
  }

  openDeleteDialog(id: string, name: string, address: string): void {
    const dialogRef = this.dialog.open(DeleteBranchComponent, {
      width: '700px',
      data: {
        id,
        name,
        address
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh();
    });
  }
}

@Component({
  selector: 'edit-branches',
  templateUrl: './edit.branches.component.html',
  styleUrls: ['./branches.component.css']
})
export class EditBranchesComponent implements OnInit {
  branch: Branch = new Branch();
  editForm: FormGroup;

  legalParties: LegalParty[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<BranchesComponent>,
    private branchService: BranchService,
    private formBuilder: FormBuilder,
    private router: Router,
    private notificationDialog: ModalNotificationService,
    private legalPartyService: LegalPartyService) {
    this.legalPartyService.getAll().subscribe(data => this.legalParties = data);
  }

  idControl = new FormControl('', [Validators.required]);
  nameControl = new FormControl('', [Validators.required]);
  legalPartyControl = new FormControl('', [Validators.required]);
  addressControl = new FormControl('', [Validators.required]);
  normativeControl = new FormControl();

  onNoClick(event): void {
    console.log(event);
    this.dialogRef.close();
  }

  get f() {
    return this.editForm.controls;
  }

  onSave(event): void {
    console.log(event);
    if (this.data.new) {
      this.branchService.create(this.f.name.value, this.f.legalParty.value, this.f.address.value, this.f.normative.value)
        .subscribe(
          data => {
            this.router.navigate(['/branches']);
            this.notificationDialog.open("Данные подразделения сохранены", true);
          },
          error => {
            console.log(error);
          }
        );
    } else {
      this.branchService.update(this.data.id, this.f.name.value, this.f.legalParty.value, this.f.address.value, this.f.normative.value)
        .subscribe(
          data => {
            this.router.navigate(['/branches']);
            this.notificationDialog.open("Изменения сохранены", true);
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      id: [],
      name: [, Validators.required],
      legalParty: [],
      address: [, Validators.required],
      normative: []
    });
    if (!this.data.new) {
      this.branchService.getBranchById(this.data.id).subscribe(b => {
        this.branch = b[0];
        this.editForm.patchValue({
          id: this.branch._id,
          name: this.branch.name,
          legalParty: this.branch.legalParty,
          address: this.branch.address,
          normative: this.branch.normative
        });
      });
    }
  }
}

@Component({
  selector: 'delete-branches',
  templateUrl: './delete.branches.component.html',
  styleUrls: ['./branches.component.css']
})
export class DeleteBranchComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<BranchesComponent>,
    private branchService: BranchService,
    private formBuilder: FormBuilder,
    private router: Router,
    private notificationDialog: ModalNotificationService
  ) {

  }

  ngOnInit(): void {
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onDelete() {
    this.branchService.delete(this.data.id)
      .subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/branches']);
          this.notificationDialog.open("Подразделение удалено", true);
        },
        error => {
          console.log(error);
        }
      );
  }

}
