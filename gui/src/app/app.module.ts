import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MatButtonModule, MatDialogModule, MatInputModule, MatSelectModule, MatTableModule} from '@angular/material';

import {LeftNavComponent} from './left-nav/left-nav.component';
import {HeaderComponent} from './header/header.component';
import {ContentComponent} from './content/content.component';
import {NewReportComponent} from './new-report/new-report.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {BranchesComponent, DeleteBranchComponent, EditBranchesComponent} from './branches/branches.component';
import {MatListModule} from '@angular/material/list';
import {DeleteUserComponent, EditUsersComponent, ResetUserComponent, UsersComponent} from './users/users.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {StatsComponent} from './stats/stats.component';
import {ViewReportComponent} from './view-report/view-report.component';
import {MatCardModule} from '@angular/material/card';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ExcelReportComponent} from './excel-report/excel-report.component';
import {ErrorInterceptor} from './_helpers/error.interceptor';
import {JwtInterceptor} from './_helpers/jwt.interceptor';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {ReceivedComponent} from './received/received.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {ModalNotificationService} from "./modalNotifications/modal.notification.service";
import {ModalNotificationComponent} from "./modalNotifications/modal.notification.component";
import { CatalogComponent } from './catalog/catalog.component';

@NgModule({
  declarations: [
    AppComponent,
    LeftNavComponent,
    HeaderComponent,
    ContentComponent,
    BranchesComponent,
    UsersComponent,
    NewReportComponent,
    StatsComponent,
    ViewReportComponent,
    NewReportComponent,
    EditBranchesComponent,
    DeleteBranchComponent,
    EditUsersComponent,
    DeleteUserComponent,
    ResetUserComponent,
    ExcelReportComponent,
    LoginComponent,
    HomeComponent,
    ReceivedComponent,
    ModalNotificationComponent,
    CatalogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatTableModule,
    MatListModule,
    MatExpansionModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatIconModule,
    MatCardModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ],
  entryComponents: [
    EditBranchesComponent,
    DeleteBranchComponent,
    EditUsersComponent,
    DeleteUserComponent,
    ResetUserComponent,
    ModalNotificationComponent
  ],
  providers: [
    FormBuilder,
    ModalNotificationService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
