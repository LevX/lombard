import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {User} from '../_models';
import {environment} from '@environments/environment';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class UserService {
  userRequestPrefix: string = `${environment.apiUrl}/protected/v1/crud/user`;

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<User[]> {
    return this.http.post<User[]>(`${this.userRequestPrefix}/get`, {"username": {"$ne": "admin"}});
  }

  getUserPById(id: string): Observable<User[]> {
    return this.http.post<User[]>(`${this.userRequestPrefix+"p"}/get`, {"_id": id});
  }

  getUserById(id: string): Observable<User[]> {
    return this.http.post<User[]>(`${this.userRequestPrefix}/get`, {"_id": id});
  }

  create(username: string, password: string, name: string, role: string, branches: string[]): Observable<User[]> {
    return this.http.post<User[]>(`${this.userRequestPrefix}/put`, {
      "username": username,
      "password": password,
      "name": name,
      "roles": [role],
      "branches": branches
    });
  }

  update(id: string, username: string, password: string, name: string, role: string, branches: string[]): Observable<User[]> {
    return this.http.post<User[]>(`${this.userRequestPrefix}/put`, {
      "_id": id,
      "username": username,
      "password": password,
      "name": name,
      "roles": [role],
      "branches": branches
    });
  }

  delete(id: string): Observable<User[]> {
    return this.http.post<User[]>(`${this.userRequestPrefix}/delete`, {
      "_id": id
    });
  }
}
