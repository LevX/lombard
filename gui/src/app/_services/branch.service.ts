import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Branch} from '../_models';
import {environment} from '@environments/environment';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class BranchService {
  constructor(private http: HttpClient) {
  }

  getAllWithUsers(): Observable<Branch[]> {
    return this.http.get<Branch[]>(`${environment.apiUrl}/protected/v1/branchWithUsers`, {});
  }

  getAll(): Observable<Branch[]> {
    return this.http.post<Branch[]>(`${environment.apiUrl}/protected/v1/crud/branch/get`, {});
  }

  getBranchesByIDs(ids: string[]): Observable<Branch[]> {
    return this.http.post<Branch[]>(`${environment.apiUrl}/protected/v1/crud/branch/get`, {"_id": {"$in": ids}});
  }

  getBranchById(id: string): Observable<Branch[]> {
    return this.http.post<Branch[]>(`${environment.apiUrl}/protected/v1/crud/branch/get`, {
      "_id": id
    });
  }

  create(name: string, legalParty: string, address: string, normative: number): Observable<Branch[]> {
    return this.http.post<Branch[]>(`${environment.apiUrl}/protected/v1/crud/branch/put`, {
      "name": name,
      "legalParty": legalParty,
      "address": address,
      "normative": normative
    });
  }

  update(id: string, name: string, legalParty: string, address: string, normative: string): Observable<Branch[]> {
    return this.http.post<Branch[]>(`${environment.apiUrl}/protected/v1/crud/branch/put`, {
      "_id": id,
      "name": name,
      "legalParty": legalParty,
      "address": address,
      "normative": normative
    });
  }

  delete(id: string): Observable<Branch[]> {
    return this.http.post<Branch[]>(`${environment.apiUrl}/protected/v1/crud/branch/delete`, {
      "_id": id
    });
  }
}
