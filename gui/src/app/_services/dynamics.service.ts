import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environments/environment';
import {Dynamics} from "../_models/dynamics";
import {Observable} from 'rxjs';
import {Tools} from "../_helpers/tools";

@Injectable({providedIn: 'root'})
export class DynamicsService {
  requestPrefix: string = `${environment.apiUrl}/protected/v1/calculateDynamics`;

  constructor(private http: HttpClient) {
  }

  getDynamics(branchId: string, reportDate: string): Observable<Dynamics> {
    return this.http.post<Dynamics>(`${this.requestPrefix}`, {
      "branchId": branchId,
      "reportDate": Tools.getTimestamp(reportDate, 0, 0, 0, 0).toString()
    });
  }
}
