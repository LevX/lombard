import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environments/environment';
import {Observable} from 'rxjs';
import {LegalParty} from '../_models/legalParty';
import {Tools} from '../_helpers/tools';
import {TreasuryInfo} from '../_models/treasuryInfo';

@Injectable({providedIn: 'root'})
export class LegalPartyService {
  legalPartyRequestPrefix: string = `${environment.apiUrl}/protected/v1/crud/legalparty`;
  bankDataRequestPrefix: string = `${environment.apiUrl}/protected/v1/crud/treasury`;
  private qqq: Observable<LegalParty[]>;

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<LegalParty[]> {
    console.log('getting all');
    return this.http.post<LegalParty[]>(`${this.legalPartyRequestPrefix}/get`, {});
  }

  create(name: string, normative: string): Observable<LegalParty[]> {
    console.log('creating legal party');
    console.log('name:' + name);
    return this.http.post<LegalParty[]>(`${this.legalPartyRequestPrefix}/put`,
      {
        'name': name,
        'normative': normative
      });
  }

  update(id: string, name: string, normative: string): Observable<LegalParty[]> {
    console.log('updating legal party');
    console.log('id:' + id);
    console.log('name:' + name);
    return this.http.post<LegalParty[]>(`${this.legalPartyRequestPrefix}/put`,
      {
        '_id': id,
        'name': name,
        'normative': normative
      });
  }

  delete(id: string): Observable<LegalParty[]> {
    console.log('deleting');
    console.log('id:' + id);
    return this.http.post<LegalParty[]>(`${this.legalPartyRequestPrefix}/delete`, {'_id': id});
  }

  getBankDataForDate(date: string): Observable<TreasuryInfo[]> {
    console.log(Tools.getTimestamp(date, 0, 0, 0, 0));
    return this.http.post<TreasuryInfo[]>(`${this.bankDataRequestPrefix}/get`, {'date': Tools.getTimestamp(date, 23, 59, 59, 0)});
  }
}
