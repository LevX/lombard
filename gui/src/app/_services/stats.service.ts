import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environments/environment';
import {BranchStats} from "../_models/branchStats";
import {OperReport} from "../_models/operReport";
import {Observable} from 'rxjs';
import {YearStats} from "../_models/yearStats";
import {TreasuryReport} from "../_models/treasuryReport";
import {TreasuryInfo} from "../_models/treasuryInfo";

@Injectable({providedIn: 'root'})
export class StatsService {
  requestPrefix: string = `${environment.apiUrl}/protected/v1/`;

  constructor(private http: HttpClient) {
  }

  getStats(year: number): Observable<BranchStats[]> {
    return this.http.get<BranchStats[]>(this.requestPrefix + 'statistics/' + year);
  }

  getOperReport(date: string): Observable<OperReport[]> {
    var requestDate: string;
    if (date != null && date != '') {
      requestDate = this.getTimestamp(date).toString();
    } else {
      requestDate = new Date().getTime().toString();
    }
    return this.http.get<OperReport[]>(this.requestPrefix + 'dailyReport/' + requestDate);
  }

  getTreasuryReport(date: string): Observable<TreasuryReport[]> {
    var requestDate: string;
    if (date != null && date != '') {
      requestDate = this.getTimestamp(date).toString();
    } else {
      requestDate = new Date().getTime().toString();
    }
    return this.http.get<TreasuryReport[]>(this.requestPrefix + 'treasuryReport/' + requestDate);
  }

  getTotalPercent(reportId: string): Observable<string> {
    return this.http.get<string>(this.requestPrefix + 'totalPercent/' + reportId);
  }

  getAvailableYears(): Observable<YearStats[]> {
    return this.http.get<YearStats[]>(this.requestPrefix + 'availableYears');
  }

  getTimestamp(date: string): number {
    let d = new Date(date).setHours(23, 59, 59, 0);
    return new Date(d).getTime() - (new Date().getTimezoneOffset() * 60 * 1000);
  }

  updateTreasury(oldId: string, id: string, date: string, value: number) {
    return this.http.post<TreasuryInfo>(this.requestPrefix + 'crud/treasury/put', {
      "_id": oldId,
      "legalPartyId": id,
      "date": this.getTimestamp(date),
      "amount": value
    });
  }

  saveTreasury(id: string, date: string, value: number) {
    return this.http.post<TreasuryInfo>(this.requestPrefix + 'crud/treasury/put', {
      "legalPartyId": id,
      "date": this.getTimestamp(date),
      "amount": value
    });
  }
}
