import {environment} from '@environments/environment';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Tools} from "../_helpers/tools";

@Injectable({providedIn: 'root'})
export class ExcelService {
  requestPrefix: string = `${environment.apiUrl}/protected/v1/report/export/excel`;

  constructor(private http: HttpClient) {
  }

  public getExcelReport(branchId: string, dateFrom: string, dateTo: string): Observable<any> {
    return this.http.post<any>(`${this.requestPrefix}`,
      {
        branchId: branchId,
        dateFrom: Tools.getTimestamp(dateFrom, 23, 59, 59, 0),
        dateTo: Tools.getTimestamp(dateTo, 23, 59, 59, 0)
      },
      {
        responseType: 'blob' as 'json',
        headers:
          new HttpHeaders().append('Content-Type', "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8")
      });
  }
}
