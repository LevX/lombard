import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environments/environment';
import {Report} from "../_models/report";
import {Expense} from "../_models/expense";
import {Observable} from 'rxjs';
import {Tools} from "../_helpers/tools";

@Injectable({providedIn: 'root'})
export class ReportService {
  requestPrefix: string = `${environment.apiUrl}/protected/v1/crud/report`;

  constructor(private http: HttpClient) {

  }

  create(
    id: string,
    branch: string,
    date: string,
    loanersPawned: string,
    loanersBought: string,
    loanedRub: string,
    repayedRub: string,
    percentRecieved: string,
    // Золото
    goldBought: string,
    goldSold: string,
    // Серебро
    silverBought: string,
    silverSold: string,
    // Бриллианты
    diamondBought: string,
    diamondSold: string,
    // Вещи
    goodsBought: string,
    goodsSold: string,
    // Касса
    cashboxEvening: string,
    // Торги
    tradesActive: string,
    goldTradeSum: string,
    goldTradeWeight: string,
    silverTradeSum: string,
    silverTradeWeight: string,
    diamondsTradeWeight: string,
    goodsTradeSum: string,
    expenses: Expense[],
    auctionAmount: string,
    version: number
  ): Observable<Report[]> {
    console.log("id:" + id);
    console.log("branch: " + branch);
    console.log("date: " + Tools.getTimestamp(date, 0, 0, 0, 0));
    console.log("loanersPawned: " + loanersPawned);
    console.log("loanersBought: " + loanersBought);
    console.log("loanedRub: " + loanedRub);
    console.log("repayedRub: " + repayedRub);
    console.log("percentRecieved: " + percentRecieved);
    // Золото
    console.log("goldBought: " + goldBought);
    console.log("goldSold: " + goldSold);
    // Серебро
    console.log("silverBought: " + silverBought);
    console.log("silverSold: " + silverSold);
    // Бриллианты
    console.log("diamondBought: " + diamondBought);
    console.log("diamondSold: " + diamondSold);
    // Вещи
    console.log("goodsBought: " + goodsBought);
    console.log("goodsSold: " + goodsSold);
    // Касса
    console.log("cashboxEvening: " + cashboxEvening);
    // Торги
    console.log("tradesActive: " + tradesActive);
    console.log("goldTradeSum: " + goldTradeSum);
    console.log("goldTradeWeight: " + goldTradeWeight);
    console.log("silverTradeSum: " + silverTradeSum);
    console.log("silverTradeWeight: " + silverTradeWeight);
    console.log("diamondsTradeWeight: " + diamondsTradeWeight);
    console.log("goodsTradeSum: " + goodsTradeSum);
    console.log("expenses: " + expenses);
    console.log("auctionAmount: " + expenses);
    console.log("version: " + version);

    let userJSON = JSON.parse(localStorage.getItem('currentUser'));
    let body = {
      "branch": branch,
      "user": userJSON._id,
      "date": Tools.getTimestamp(date, 0, 0, 0, 0),
      "loanersPawned": loanersPawned,
      "loanersBought": loanersBought,
      "loanedRub": loanedRub,
      "repayedRub": repayedRub,
      "percentRecieved": percentRecieved,
      // Золото
      "goldBought": goldBought,
      "goldSold": goldSold,
      // Серебро
      "silverBought": silverBought,
      "silverSold": silverSold,
      // Бриллианты
      "diamondBought": diamondBought,
      "diamondSold": diamondSold,
      // Вещи
      "goodsBought": goodsBought,
      "goodsSold": goodsSold,
      // Касса
      "cashboxEvening": cashboxEvening,
      // Торги
      "tradesActive": tradesActive,
      "goldTradeSum": goldTradeSum,
      "goldTradeWeight": goldTradeWeight,
      "silverTradeSum": silverTradeSum,
      "silverTradeWeight": silverTradeWeight,
      "diamondsTradeWeight": diamondsTradeWeight,
      "goodsTradeSum": goodsTradeSum,
      "expenses": expenses,
      "auctionAmount": auctionAmount,
      "version": version
    };
    if (id != '') {
      body["_id"] = id;
    }
    console.log(body);
    return this.http.post<Report[]>(`${this.requestPrefix}/put`, JSON.parse(JSON.stringify(body)));
  }

  getAllReports(): Observable<Report[]> {
    let userJSON = JSON.parse(localStorage.getItem('currentUser'));
    return this.http.post<Report[]>(`${this.requestPrefix}/get`, {"user": userJSON._id});
  }

  getReportsByDate(date: string): Observable<Report[]> {
    console.log("date: " + Tools.getTimestamp(date, 0, 0, 0, 0));
    return this.http.post<Report[]>(`${this.requestPrefix}/get`, {"date": Tools.getTimestamp(date, 0, 0, 0, 0)});
  }

  getReportByDateAndBranch(date: string, branch: string): Observable<Report[]> {
    console.log("date: " + Tools.getTimestamp(date, 0, 0, 0, 0));
    console.log("branch: " + branch);
    return this.http.post<Report[]>(`${this.requestPrefix}/get`, {
      "branch": branch,
      "date": Tools.getTimestamp(date, 0, 0, 0, 0)
    });
  }
}
