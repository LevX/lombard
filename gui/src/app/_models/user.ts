export class User {
  _id: string;
  username: string;
  password: string;
  name: string;
  roles: string[];
  branches: string[];
  token?: string;
}
