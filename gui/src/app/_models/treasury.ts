import {TreasuryReport} from "./treasuryReport";
import {TreasuryInfo} from "./treasuryInfo";

export class Treasury {
  _id: string;
  legalParty: string;
  loanedTodaySum: number;
  repayedTodaySum: number;
  balanceSum: number;
  cashboxSum: number;
  normativeSum: number;
  differenceSum: number;
  reports: TreasuryReport[];
  treasury:TreasuryInfo;
}
