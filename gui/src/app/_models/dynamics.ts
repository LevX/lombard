export class Dynamics {
  loanersAsset: number;
  volume: number;
  goldBalance: number;
  silverBalance: number;
  diamondBalance: number;
  goodsBalance: number;
  cashboxEvening: number;
}
