import {User} from "./user";

export class Branch {
  _id: string;
  name: string;
  legalParty: string;
  address: string;
  normative: string;
  users: User[];
}
