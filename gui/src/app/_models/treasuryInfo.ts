export class TreasuryInfo {
  _id: string;
  legalPartyId: string;
  date: Date;
  amount: number;
}
