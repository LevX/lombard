import {TreasuryInfo} from "./treasuryInfo";

export class TreasuryReport {
  legalParty: string;
  legalNormative: number;
  loanedTodaySum: number;
  repayedTodaySum: number;
  balanceSum: number;
  cashboxSum: number;
  normativeSum: number;
  differenceSum: number;
  reports: TreasuryReport[];
  treasury: TreasuryInfo;
}
