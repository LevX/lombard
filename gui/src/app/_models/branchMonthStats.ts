export class BranchMonthStats {
  cashboxEndMorning: number;
  cashboxStartMorning: number;
  endBasket: number;
  monthAverageBasket: number;
  monthExpenses: number;
  monthLoanRub: number;
  monthNum: number;
  monthRepayRub: number;
  monthTradeBalance: number;
  monthTradeSum: number;
  startBasket: number;
  tradeIncome: number;
  monthName: string;
}
