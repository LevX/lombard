export class OperReport {
  perBranch: OperBranch[];
  totalLoanedToday: string;
  totalRepayedToday: string;
  totalBalance: string;
  totalCashBox: string;
}

export class OperBranch {
  branchName: string;
  loanedToday: string;
  repayedToday: string;
  balance: string;
  cashbox: string;
}
