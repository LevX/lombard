import {BranchMonthStats} from "./branchMonthStats";
import {Branch} from "./branch";

export class BranchStats {
    branchInfo: Branch;
    monthlyReports: BranchMonthStats;
}
