import {Expense} from '@app/_models/expense';

export class Report {
  _id: string;
  branch: string;
  user: string;
  date: Date;
  //Заемщики
  loanersPawned: number;
  loanersBought: number;
  loanersAsset: number;
  // Корзина
  loanedRub: number;
  repayedRub: number;
  percentRecieved: number;
  // Золото
  goldBought: number;
  goldSold: number;
  // Серебро
  silverBought: number;
  silverSold: number;
  // Бриллианты
  diamondBought: number;
  diamondSold: number;
  //Касса
  cashboxEvening: number;
  // Вещи
  goodsBought: number;
  goodsSold: number;
  // Торги
  tradesActive: number;
  goldTradeSum: number;
  goldTradeWeight: number;
  silverTradeSum: number;
  silverTradeWeight: number;
  diamondsTradeWeight: number;
  goodsTradeSum: number;
  // Расходы
  expenses: Expense[];

  auctionAmount: number;

  version: number;
}
