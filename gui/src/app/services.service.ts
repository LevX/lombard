import {Injectable} from '@angular/core';
import {GlobalsService} from "./globals.service";

@Injectable({
  providedIn: 'root'
})
export class Services {

  constructor(private globals: GlobalsService) {
  }

  public validateSession(): boolean {
    let username = GlobalsService._username;
    let password = GlobalsService._password;

    console.log(GlobalsService._username);
    console.log(GlobalsService._password);
    if (username == "admin" && password == "admin") {
      return true;
    }
    return false;
  }

  public invalidateSession() {
    GlobalsService._username = "";
    GlobalsService._password = "";
  }

  public getUserName(): string {
    return "Иванов И. И.";
  }
}
