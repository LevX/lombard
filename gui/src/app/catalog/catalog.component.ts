import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {LegalParty} from '../_models/legalParty';
import {FormControl, FormGroup} from '@angular/forms';
import {LegalPartyService} from '../_services/legalParty_service';
import {BranchService} from '../_services/branch.service';
import {Branch} from '../_models';
import {Tools} from '../_helpers/tools';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {
  normativesVisible: boolean = false;
  legalPartiesVisible: boolean = false;
  isDelete: boolean = false;
  loading: boolean = false;

  disable_normative = true;
  normative_color = 'primary';
  legal_parties_color = 'primary';

  legalParties: LegalParty[];
  branches: Branch[];

  normativeText = 'Норматив';
  disableCancel = true;

  // @ts-ignore
  @ViewChild('normative') normativeInput: ElementRef;

  addLegalPartyFormGroup: FormGroup = new FormGroup({
    id: new FormControl(),
    legalPartyName: new FormControl(),
    legalPartyNormative: new FormControl()
  });

  saveNormativeFormGroup: FormGroup = new FormGroup(
    {
      id: new FormControl(),
      name: new FormControl(),
      address: new FormControl(),
      legalParty: new FormControl(),
      normative: new FormControl()
    }
  );

  constructor(private legalPartyService: LegalPartyService, private branchService: BranchService) {
    this.normativesVisible = false;
    this.legalPartiesVisible = false;
  }

  ngOnInit() {
  }

  updateLegalParties(): void {
    this.legalPartiesForm.id.patchValue('');
    this.legalPartiesForm.legalPartyName.patchValue('');
    this.legalPartiesForm.legalPartyNormative.patchValue('');
    this.isDelete = false;
    this.loading = true;
    this.legalPartyService.getAll().subscribe(data => {
      this.legalParties = data;
      this.loading = false;
    });
  }

  updateNormatives(): void {
    this.branchService.getAll().subscribe(data => {
      this.branches = data;
    });
  }

  showLegalParties(): void {
    this.normativesVisible = false;
    this.legalPartiesVisible = true;
    this.normative_color = 'primary';
    this.legal_parties_color = 'warn';
    this.updateLegalParties();
  }

  showNormatives(): void {
    this.normativesVisible = true;
    this.legalPartiesVisible = false;
    this.normative_color = 'warn';
    this.legal_parties_color = 'primary';
    this.updateNormatives();
  }


  get legalPartiesForm() {
    return this.addLegalPartyFormGroup.controls;
  }

  get normativesForm() {
    return this.saveNormativeFormGroup.controls;
  }

  editLegalParty(id: string, name: string, normative: string) {
    this.legalPartiesForm.id.patchValue(id);
    this.legalPartiesForm.legalPartyName.patchValue(name);
    this.legalPartiesForm.legalPartyNormative.patchValue(normative);
    this.isDelete = false;
  }

  editNormative(branch: Branch): void {
    this.disable_normative = false;
    this.normativesForm.id.patchValue(branch._id);
    this.normativesForm.name.patchValue(branch.name);
    this.normativesForm.address.patchValue(branch.address);
    this.normativesForm.legalParty.patchValue(branch.legalParty);
    this.normativesForm.normative.patchValue(branch.normative);
    this.normativeText = 'Норматив для "' + branch.name + '"';
    this.normativeInput.nativeElement.focus();
  }

  cancelNormative(): void {
    this.disable_normative = true;
    this.normativesForm.id.patchValue('');
    this.normativesForm.name.patchValue('');
    this.normativesForm.address.patchValue('');
    this.normativesForm.legalParty.patchValue('');
    this.normativesForm.normative.patchValue('');
    this.normativeText = 'Норматив';
  }

  deleteLegalParty(id: string, name: string, normative: string) {
    this.editLegalParty(id, name, normative);
    this.isDelete = true;
  }

  cancelEdit() {
    this.legalPartiesForm.id.patchValue('');
    this.legalPartiesForm.legalPartyName.patchValue('');
    this.legalPartiesForm.legalPartyNormative.patchValue('');
    this.isDelete = false;
  }

  onModifyNormative() {
    if (this.normativesForm.id.value != null && this.normativesForm.id.value != '') {
      this.branchService.update(
        this.normativesForm.id.value,
        this.normativesForm.name.value,
        this.normativesForm.legalParty.value,
        this.normativesForm.address.value,
        Tools.getValue(this.normativesForm.normative.value)
      ).subscribe(data => console.log(data),
        error => {
          console.log(error);
          this.updateNormatives();
        });
      this.cancelNormative();
    }
  }

  public keyDown(event, allowDecimals: boolean) {
    return Tools.allowNumbers(event, allowDecimals);
  }

  public keyUp(event, allowEmpty) {
    Tools.keyUpProcessor(event, allowEmpty);
  }

  getValue(value: string): string {
    if (value == '') {
      return '0';
    }
    return value.replace(/\s/g, '').replace(/,/g, '.');
  }

  onBlurFixed(event, allowEmpty) {
    if (allowEmpty && event.target.value == '') {
      return;
    }
    event.target.value = Tools.strToNum(event.target.value).toLocaleString('ru-RU');
  }

  addSeparators(value: string): string {
    if (value != null && value != '') {
      return parseFloat(value).toLocaleString('ru-RU');
    } else {
      return value;
    }
  }

  onModifyLegalParty() {
    console.log('modifying');
    if (this.legalPartiesForm.id.value == null || this.legalPartiesForm.id.value == '') {
      if (this.legalPartiesForm.legalPartyName.value != null && this.legalPartiesForm.legalPartyName.value != '') {
        this.legalPartyService.create(
          this.legalPartiesForm.legalPartyName.value,
          Tools.getValue(this.legalPartiesForm.legalPartyNormative.value)
        ).subscribe(
          data => console.log(data),
          error => {
            console.log(error);
            this.updateLegalParties();
          });
      }
    } else {
      if (this.isDelete) {
        this.legalPartyService.delete(this.legalPartiesForm.id.value).subscribe(
          data => {
            console.log(data);
            this.updateLegalParties();
          },
          error => {
            console.log(error);
            this.updateLegalParties();
          }
        );
      } else {
        if (this.legalPartiesForm.legalPartyName.value != null && this.legalPartiesForm.legalPartyName.value != '') {
          this.legalPartyService.update(
            this.legalPartiesForm.id.value,
            this.legalPartiesForm.legalPartyName.value,
            Tools.getValue(this.legalPartiesForm.legalPartyNormative.value)
          ).subscribe(
            data => console.log(data),
            error => {
              console.log(error);
              this.updateLegalParties();
            });
        }
      }
    }
  }


}
