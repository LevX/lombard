import {Component, Inject, OnInit} from '@angular/core';
import {BranchesComponent} from '../branches/branches.component';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {BranchService} from "../_services/branch.service";
import {UserService} from "../_services/user.service";
import {Branch, User} from "../_models";
import {ModalNotificationService} from "../modalNotifications/modal.notification.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private dialog: MatDialog, private userService: UserService, private branchService: BranchService) {
    this.refresh();
  }

  USER_DATA: User[];
  dataSource = this.USER_DATA;
  BRANCH_DATA: Branch[];
  bdataSource = this.BRANCH_DATA;

  displayedColumns: string[] = ['login', 'name', 'roles', 'branches', 'actions'];
  displayBranchNames: string[] = ['branchName'];

  ngOnInit() {
  }

  refresh() {
    this.branchService.getAll().subscribe(branches => {
      this.bdataSource = branches;
    });
    this.userService.getAll().subscribe(users => {
      this.dataSource = users;
    });
  }

  getBranchName(id: string): string {
    let foundValue = "[" + id + "]";
    this.bdataSource.forEach(b => {
      if (b['_id'] == id) {
        foundValue = b['name'];
      }
    });
    return foundValue;
  }

  openAddDialog(): void {
    const dialogRef = this.dialog.open(EditUsersComponent, {
      width: '700px',
      data: {
        new: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh();
    });
  }

  openEditDialog(id: string): void {
    const dialogRef = this.dialog.open(EditUsersComponent, {
      width: '700px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh();
    });
  }

  openPasswordResetDialog(id: string, login: string, name: string) {
    const dialogRef = this.dialog.open(ResetUserComponent, {
      width: '700px',
      data: {
        id: id,
        name: name,
        login: login
      }
    })
  }

  openDeleteDialog(id: string, userName: string, name: string): void {
    const dialogRef = this.dialog.open(DeleteUserComponent, {
      width: '700px',
      data: {
        id,
        userName,
        name
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh();
    });
  }
}

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit.users.component.html',
  styleUrls: ['./users.component.css']
})
export class EditUsersComponent implements OnInit {
  user: User = new User();

  BRANCH_DATA: Branch[];
  branches = this.BRANCH_DATA;

  editForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<BranchesComponent>,
    private userService: UserService,
    private branchService: BranchService,
    private formBuilder: FormBuilder,
    private router: Router,
    private notificationDialog: ModalNotificationService) {
    this.branchService.getAll().subscribe(branches => {
      this.branches = branches;
      console.log(branches);
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get f() {
    return this.editForm.controls;
  }

  onSave(): void {
    if (this.data.new) {
      this.userService.create(this.f.login.value, this.f.password.value, this.f.name.value, this.f.roles.value, this.f.branches.value)
        .subscribe(
          data => {
            this.dialogRef.close();
            this.notificationDialog.open("Пользователь сохранён", true);
          },
          error => {
            if (error == "OK") {
              this.dialogRef.close();
              this.notificationDialog.open("Пользователь сохранён", true);
            }
            console.log(error);
          }
        );
    } else {
      this.userService.update(this.data.id, this.f.login.value, this.f.password.value, this.f.name.value, this.f.roles.value, this.f.branches.value)
        .subscribe(
          data => {
            console.log("updated");
            this.dialogRef.close();
            this.notificationDialog.open("Изменения сохранены", true);
          },
          error => {
            if (error == "OK") {
              this.dialogRef.close();
              this.notificationDialog.open("Изменения сохранены", true);
            }
          }
        );
    }
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      id: [],
      login: [, Validators.required],
      password: [, Validators.required],
      name: [, Validators.required],
      roles: [, Validators.required],
      branches: [, Validators.required]
    });
    if (!this.data.new) {
      this.userService.getUserPById(this.data.id).subscribe(u => {
        this.user = u[0];
        console.log(this.user);
        this.editForm.patchValue({
          id: this.user._id,
          login: this.user.username,
          password: this.user.password,
          name: this.user.name,
          roles: this.user.roles[0],
          branches: this.user.branches
        });
      });
    }
  }
}

@Component({
  selector: 'app-delete-users',
  templateUrl: './delete.users.component.html',
  styleUrls: ['./users.component.css']
})
export class DeleteUserComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<BranchesComponent>,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router,
    private notificationDialog: ModalNotificationService
  ) {

  }

  ngOnInit(): void {
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onDelete() {
    this.userService.delete(this.data.id).subscribe(
      data => {
        console.log(data);
        this.dialogRef.close();
        this.notificationDialog.open("Пользователь удалён", true);
      },
      error => {
        if (error == 'OК') {
          this.dialogRef.close();
          this.notificationDialog.open("Пользователь удалён", true);
        }
        console.log(error);
      }
    );
  }
}

@Component({
  selector: 'app-reset-users',
  templateUrl: './reset.users.component.html',
  styleUrls: ['./users.component.css']
})
export class ResetUserComponent implements OnInit {
  newPassword: string;
  user: User = new User();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<BranchesComponent>,
    private userService: UserService,
  ) {

  }

  ngOnInit(): void {
    this.newPassword = this.generatePassword();
    ['test']
    this.userService.getUserById(this.data.id).subscribe(u => {
      this.userService.update(this.data.id, u[0].username, this.newPassword, u[0].name, u[0].roles[0], u[0].branches)
        .subscribe(
          data => {
            console.log(data);
          }, error => {
            console.log(error);
          });
    });
  }

  onClose() {
    this.dialogRef.close();
  }

  generatePassword(): string {
    let s1 = "aeiouy";
    let s2 = "bcdfghjklmnpqrstvwxz";
    let newPassword = "";
    for (let i = 0; i < 5; i++) {
      newPassword = newPassword + s2.substr(Math.floor(Math.random() * s2.length), 1);
      newPassword = newPassword + s1.substr(Math.floor(Math.random() * s1.length), 1);
    }
    return newPassword;
  }
}
