import {Component} from '@angular/core';
import {User} from './_models';
import {AuthenticationService} from "./_services/authentication.service";
import {Tools} from "./_helpers/tools";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  currentUser: User;

  constructor(authServise: AuthenticationService) {
    authServise.currentUser.subscribe(u => this.currentUser = u);
    Tools.updateLocale();
  }

}
