import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalsService {

  constructor() {
  }

  public static _username: string;
  public static _password: string;

  public setUsername(username: string) {
    GlobalsService._username = username;
  }

  public setPassword(password: string) {
    GlobalsService._password = password;
  }
}
