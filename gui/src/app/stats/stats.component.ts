import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {BranchService} from "../_services/branch.service";
import {StatsService} from "../_services/stats.service";
import {Branch} from "../_models";
import {BranchStats} from "../_models/branchStats";
import {OperReport} from "../_models/operReport";
import {YearStats} from "../_models/yearStats";
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TreasuryReport} from "../_models/treasuryReport";
import {DateFormats} from "../_helpers/dateFormats";
import {ModalNotificationService} from "../modalNotifications/modal.notification.service";
import {Tools} from "../_helpers/tools";

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DateFormats}
  ]
})
export class StatsComponent implements OnInit {
  @ViewChild('container', {read: ViewContainerRef, static: true}) container: ViewContainerRef;

  loading = false;
  branches: Branch[];
  branchStats: BranchStats[];
  operReport: OperReport[];
  treasuryReport: TreasuryReport[];
  treasuryTotals: TreasuryReport = new TreasuryReport();
  years: YearStats[];
  selectedYear: number = 0;
  currentYear: number;

  operColor: string = "primary";
  treasuryColor: string = "primary";
  statsColor: string = "primary";

  operDateForm: FormGroup = new FormGroup({
    date: new FormControl(new Date(), Validators.required),
  });

  treasuryDateForm: FormGroup = new FormGroup({
    date: new FormControl(new Date(), Validators.required),
  });

  constructor(private branchService: BranchService, private statsService: StatsService, private notificationDialog: ModalNotificationService) {
    this.statsService.getAvailableYears().subscribe(
      data => {
        this.years = data;
        console.log(data);
      });
    this.currentYear = new Date().getFullYear();
  }

  showOper(): void {
    this.operColor = "warn";
    this.treasuryColor = "primary";
    this.statsColor = "primary";
    this.updateOperReport();
  }

  showTreasury(): void {
    this.operColor = "primary";
    this.treasuryColor = "warn";
    this.statsColor = "primary";
    this.updateTreasuryReport();
  }

  showStats(): void {
    this.operColor = "primary";
    this.treasuryColor = "primary";
    this.statsColor = "warn";
  }

  updateTreasuryReport() {
    this.treasuryTotals = new TreasuryReport();
    this.treasuryTotals.legalNormative = 0;
    this.treasuryTotals.loanedTodaySum = 0;
    this.treasuryTotals.repayedTodaySum = 0;
    this.treasuryTotals.balanceSum = 0;
    this.treasuryTotals.cashboxSum = 0;
    this.treasuryTotals.normativeSum = 0;
    this.treasuryTotals.differenceSum = 0;
    this.statsService.getTreasuryReport(this.treasuryDateForm.controls.date.value).subscribe(
      treasury => {
        this.treasuryReport = treasury;
        console.log(treasury);
      });
    this.statsService.getTreasuryReport(this.treasuryDateForm.controls.date.value).subscribe(
      treasury => {
        for (var i = 0; i < treasury.length; i++) {
          console.log(treasury[i]);
          this.treasuryTotals.legalNormative += treasury[i].legalNormative;
          this.treasuryTotals.loanedTodaySum += treasury[i].loanedTodaySum;
          this.treasuryTotals.repayedTodaySum += treasury[i].repayedTodaySum;
          this.treasuryTotals.balanceSum += treasury[i].balanceSum;
          this.treasuryTotals.cashboxSum += treasury[i].cashboxSum;
          this.treasuryTotals.normativeSum += treasury[i].normativeSum + treasury[i].legalNormative;
          this.treasuryTotals.differenceSum += treasury[i].differenceSum + treasury[i].legalNormative;
          if (treasury[i].treasury) {
            this.treasuryTotals.cashboxSum += treasury[i].treasury.amount;
            this.treasuryTotals.differenceSum += (-treasury[i].treasury.amount);
          }
          for (var j = 0; j < treasury[i].reports.length; j++) {
            console.log(treasury[i].reports[j]);
            // this.treasuryTotals.cashboxSum += treasury[i].cashboxSum;
          }
        }
      });
  }

  updateOperReport() {
    this.statsService.getOperReport(this.operDateForm.controls.date.value).subscribe(oper => {
      this.operReport = oper;
      console.log(oper);
    });
  }

  saveTreasury(id: string) {
    var oid = document.getElementsByName("id_" + id);
    var value: string = (<HTMLInputElement>document.getElementsByName("tid_" + id)[0]).value;
    var date = this.treasuryDateForm.controls.date.value;
    console.log(id + " -> " + value);
    if (oid.length > 0) {
      this.statsService.updateTreasury((<HTMLInputElement>oid[0]).value, id, date, Tools.strToNum(value)).subscribe(
        data => {
          console.log(data);
          this.updateTreasuryReport();
        },
        error => {
          console.log(error);
          this.updateTreasuryReport();
          if (error == 'OK') {
            this.notificationDialog.open("Спасибо, данные сохранены", true);
          } else {
            this.notificationDialog.open("Ошибка [" + error + "]", true);
          }
        });
    } else {
      this.statsService.saveTreasury(id, date, Tools.strToNum(value)).subscribe(
        data => {
          console.log(data);
          this.updateTreasuryReport();
        },
        error => {
          console.log(error);
          this.updateTreasuryReport();
          if (error == 'OK') {
            console.log("1");
            this.notificationDialog.open("Спасибо, данные сохранены", true);
          } else {
            console.log("2");
            this.notificationDialog.open("Ошибка [" + error + "]", true);
          }
        });
    }
  }

  showYear(year: number) {
    this.loading = true;
    this.selectedYear = year;
    this.branchStats = null;
    this.statsService.getStats(year).subscribe(stats => {
      console.log(stats);
      this.loading = false;
      if (stats.length > 0) {
        this.branchStats = stats;
      }
    });
  }

  public keyDown(event, allowDecimals: boolean) {
    Tools.allowNumbers(event, allowDecimals);
  }

  public keyUp(event, allowEmpty) {
    Tools.keyUpProcessor(event, allowEmpty);
  }

  onBlur(event) {
    event.target.value = this.formatDecimals(event.target.value);
  }

  formatDecimals(value: string): string {
    value = parseFloat(Tools.strToNum(value).toFixed(2)).toLocaleString("ru-RU");
    if (value.indexOf(",") > 0) {
      value = value + "00";
    } else {
      value = value + ",00";
    }
    return value.substring(0, value.indexOf(",") + 3);
  }

  getMonthName(monthNum: number): string {
    switch (monthNum) {
      case 1:
        return "Январь";
      case 2:
        return "Февраль";
      case 3:
        return "Март";
      case 4:
        return "Апрель";
      case 5:
        return "Май";
      case 6:
        return "Июнь";
      case 7:
        return "Июль";
      case 8:
        return "Август";
      case 9:
        return "Сентябрь";
      case 10:
        return "Октябрь";
      case 11:
        return "Ноябрь";
      case 12:
        return "Декабрь";
      default:
        console.log("месяц с номером");
    }
  }

  public getYearColor(year): string {
    if (year == this.selectedYear) {
      if (this.loading) {
        return "warn";
      }
      return "primary";
    }
    return "basic";
  }

  addSeparators(value: string): string {
    try {
      return parseFloat(value).toLocaleString("ru-RU");
    } catch (e) {
      return "0";
    }
  }

  ngOnInit() {
  }
}
