import {saveAs} from "file-saver";
import {Component, OnInit} from '@angular/core';
import {BranchService} from "../_services/branch.service";
import {Branch} from "../_models";
import {FormControl, FormGroup} from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import {ExcelService} from "../_services/excel.service";
import {ModalNotificationService} from "../modalNotifications/modal.notification.service";
import {DateFormats} from "../_helpers/dateFormats";

@Component({
  selector: 'app-excel-report',
  templateUrl: './excel-report.component.html',
  styleUrls: ['./excel-report.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DateFormats},
  ],
})
export class ExcelReportComponent implements OnInit {

  branches: Branch[];
  excelRequest: FormGroup = new FormGroup({
    branch: new FormControl(""),
    dateFrom: new FormControl(""),
    dateTo: new FormControl(""),
  });

  loading: boolean = false;

  constructor(private branchService: BranchService, private excelService: ExcelService, private notificationDialog: ModalNotificationService) {
    branchService.getAll().subscribe(data => {
      this.branches = data;
    });
    var startDate = new Date();
    startDate.setMonth(0);
    startDate.setDate(1);
    var endDate = new Date();
    endDate.setMonth(11);
    endDate.setDate(31);
    this.f.dateFrom.patchValue(startDate);
    this.f.dateTo.patchValue(endDate);
  }

  get f() {
    return this.excelRequest.controls;
  }

  saveDisabled(): boolean {
    if (this.f.branch.value == '' || this.f.dateFrom.value == null || this.f.dateTo.value == null) {
      return true;
    } else {
      return false;
    }
  }

  ngOnInit() {
  }

  onSave() {
    this.loading = true;
    this.excelService.getExcelReport(this.f.branch.value, this.f.dateFrom.value, this.f.dateTo.value).subscribe(
      data => {
        let filename = 'report.xls';
        saveAs(data, filename);
        this.loading = false;
      },
      error => {
        console.log("error:" + error);
        this.notificationDialog.open("Произошла ошибка, попробуйте выгрузить данные позже", false);
        this.loading = false;
      },
      () => {
        this.loading = false;
        console.log("save complete")
      });

  }
}
