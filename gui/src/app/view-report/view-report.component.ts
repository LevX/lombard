import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {Branch, User} from "../_models";
import {BranchService} from "../_services/branch.service";
import {ReportService} from "../_services/report.service";
import {Report} from "../_models/report";
import {DynamicsService} from "../_services/dynamics.service";
import {Dynamics} from "../_models/dynamics";
import {isUndefined} from 'util';
import {UserService} from "../_services/user.service";
import {ModalNotificationService} from "../modalNotifications/modal.notification.service";
import {StatsService} from "../_services/stats.service";
import {Tools} from "../_helpers/tools";
import {DateFormats} from "../_helpers/dateFormats";

@Component({
  selector: 'app-view-report',
  templateUrl: './view-report.component.html',
  styleUrls: ['./view-report.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DateFormats},
  ],
})
export class ViewReportComponent implements OnInit {

  report: Report;
  dynamics: Dynamics = new Dynamics();
  username: string = "";
  showReport: boolean = false;
  BRANCH_DATA: Branch[];
  branches = this.BRANCH_DATA;
  user: User = JSON.parse(localStorage.getItem("currentUser"));
  version: number;
  loadedReport: Report;
  loadingReport = false;
  loadingDynamics = false;

  viewReportForm: FormGroup = new FormGroup({
    id: new FormControl('0'),
    branch: new FormControl('', Validators.required),
    date: new FormControl(new Date(), Validators.required),
    loanersPawned: new FormControl("0"),
    loanersBought: new FormControl("0"),
    loanersAsset: new FormControl("0"),
    pawnersRate: new FormControl("0 / 0"),
    // Корзина
    loanedRub: new FormControl('0'),
    repayedRub: new FormControl('0'),
    percentRecieved: new FormControl('0'),
    volume: new FormControl('0'),
    totalPercentRecieved: new FormControl('0'),
    dailyGrowth: new FormControl('0'),
    // Золото
    goldBought: new FormControl('0.00'),
    goldSold: new FormControl('0.00'),
    goldBalance: new FormControl('0.00'),
    // Серебро
    silverBought: new FormControl('0.00'),
    silverSold: new FormControl('0.00'),
    silverBalance: new FormControl('0.00'),
    // Бриллианты
    diamondBought: new FormControl('0.00'),
    diamondSold: new FormControl('0.00'),
    diamondBalance: new FormControl('0.00'),
    // Вещи
    goodsBought: new FormControl('0'),
    goodsSold: new FormControl('0'),
    goodsBalance: new FormControl('0.00'),
    // Касса
    cashboxMorning: new FormControl('0'),
    cashboxEvening: new FormControl('0'),
    // Торги
    tradesActive: new FormControl('0'),
    goldTradeSum: new FormControl('0'),
    goldTradeWeight: new FormControl('0.00'),
    silverTradeSum: new FormControl('0'),
    silverTradeWeight: new FormControl('0.00'),
    diamondsTradeWeight: new FormControl('0.00'),
    goodsTradeSum: new FormControl('0'),
    expenseName: new FormControl(''),
    expenseSum: new FormControl(''),
    auctionAmount: new FormControl(''),
    tradeSum: new FormControl('')
  });

  constructor(private userService: UserService,
              private branchService: BranchService,
              private reportService: ReportService,
              private dynamicsService: DynamicsService,
              private statsService: StatsService,
              private cd: ChangeDetectorRef,
              private notificationDialog: ModalNotificationService) {
    let user = JSON.parse(localStorage.getItem("currentUser"));
    if (user.roles.indexOf("admin") >= 0) {
      this.branchService.getAll().subscribe(branches => {
        this.branches = branches;
      });
    } else {
      this.branchService.getBranchesByIDs(user.branches).subscribe(branches => {
        this.branches = branches;
      });
    }
  }

  get f() {
    return this.viewReportForm.controls;
  }

  ngOnInit() {

  }

  public changeDynamics(): void {
    console.log(this.dynamics);
    if (!isUndefined(this.dynamics.loanersAsset) && !isUndefined(this.loadedReport)) {
      let active = Tools.strToNum(this.f.loanersPawned.value) - Tools.strToNum(this.f.loanersBought.value) + this.dynamics.loanersAsset - Tools.strToNum(this.f.tradesActive.value);
      this.f.loanersAsset.patchValue(this.transformInput(active.toString()));
      let pNum = (this.loadedReport.loanersPawned - this.loadedReport.loanersBought - this.loadedReport.tradesActive).toString();
      let pPrc = "-";
      if (active != 0) {
        let pTmp = active - (this.loadedReport.loanersPawned - this.loadedReport.loanersBought - this.loadedReport.tradesActive);
        if (pTmp != 0) {
          pPrc = ((Math.round((this.loadedReport.loanersPawned - this.loadedReport.loanersBought - this.loadedReport.tradesActive) / (active - (this.loadedReport.loanersPawned - this.loadedReport.loanersBought - this.loadedReport.tradesActive)) * 10000) / 100)).toString();
        } else {
          pPrc = "0";
        }
      }
      this.f.pawnersRate.patchValue(this.transformInput(pNum) + " чел. / " + pPrc + " %");
    }
    if (!isUndefined(this.dynamics.volume)) {
      let finBasket = Tools.strToNum(this.f.loanedRub.value) - Tools.strToNum(this.f.repayedRub.value) - Tools.strToNum(this.f.goldTradeSum.value)
        - Tools.strToNum(this.f.silverTradeSum.value) - Tools.strToNum(this.f.goodsTradeSum.value) + this.dynamics.volume;
      this.f.volume.patchValue(this.transformInput(finBasket.toString()));
      this.f.totalPercentRecieved.patchValue("0");
      let dgRub = (Tools.strToNum(this.f.loanedRub.value) - Tools.strToNum(this.f.repayedRub.value));
      let dgPrc = "-";
      if (finBasket != 0) {
        dgPrc = (Math.round(dgRub * 10000 / finBasket) / 100).toString();
      }
      this.f.dailyGrowth.patchValue(this.transformInput(dgRub.toString()) + " руб. / " + this.transformInput(dgPrc) + " %");
    }
    if (!isUndefined(this.dynamics.goldBalance)) {
      this.f.goldBalance.patchValue(this.transformInput((Tools.strToNum(this.f.goldBought.value) - Tools.strToNum(this.f.goldSold.value) + this.dynamics.goldBalance - Tools.strToNum(this.f.goldTradeWeight.value)).toFixed(2).toString()));
    }
    if (!isUndefined(this.dynamics.silverBalance)) {
      this.f.silverBalance.patchValue(this.transformInput((Tools.strToNum(this.f.silverBought.value) - Tools.strToNum(this.f.silverSold.value) + this.dynamics.silverBalance - Tools.strToNum(this.f.silverTradeWeight.value)).toFixed(2).toString()));
    }
    if (!isUndefined(this.dynamics.diamondBalance)) {
      this.f.diamondBalance.patchValue(this.transformInput((Tools.strToNum(this.f.diamondBought.value) - Tools.strToNum(this.f.diamondSold.value) + this.dynamics.diamondBalance - Tools.strToNum(this.f.diamondsTradeWeight.value)).toFixed(2).toString()));
    }
    if (!isUndefined(this.dynamics.goodsBalance)) {
      this.f.goodsBalance.patchValue(this.transformInput((Tools.strToNum(this.f.goodsBought.value) - Tools.strToNum(this.f.goodsSold.value) + this.dynamics.goodsBalance - Tools.strToNum(this.f.goodsTradeSum.value)).toString()));
    }
    if (!isUndefined(this.dynamics.cashboxEvening)) {
      this.f.cashboxMorning.patchValue(this.transformInput(this.dynamics.cashboxEvening.toString()));
    }
    this.f.tradeSum.patchValue(this.transformInput((Tools.strToNum(this.f.goldTradeSum.value) + Tools.strToNum(this.f.silverTradeSum.value) + Tools.strToNum(this.f.goodsTradeSum.value)).toString()));
  }

  resetForm() {
    console.log("resetting form");
    this.f.id.patchValue("");
    this.f.loanersPawned.patchValue("0");
    this.f.loanersBought.patchValue("0");
    this.f.loanersAsset.patchValue("0");
    this.f.loanedRub.patchValue("0");
    this.f.repayedRub.patchValue("0");
    this.f.percentRecieved.patchValue("0");
    this.f.volume.patchValue("0");
    // Золото
    this.f.goldBought.patchValue("0");
    this.f.goldSold.patchValue("0");
    this.f.goldBalance.patchValue("0");
    // Серебро
    this.f.silverBought.patchValue("0");
    this.f.silverSold.patchValue("0");
    this.f.silverBalance.patchValue("0");
    // Бриллианты
    this.f.diamondBought.patchValue("0");
    this.f.diamondSold.patchValue("0");
    this.f.diamondBalance.patchValue("0");
    // Вещи
    this.f.goodsBought.patchValue("0");
    this.f.goodsSold.patchValue("0");
    this.f.goodsBalance.patchValue("0");
    // Касса
    this.f.cashboxMorning.patchValue("0");
    this.f.cashboxEvening.patchValue("0");
    // Торги
    this.f.tradesActive.patchValue("0");
    this.f.goldTradeSum.patchValue("0");
    this.f.goldTradeWeight.patchValue("0");
    this.f.silverTradeSum.patchValue("0");
    this.f.silverTradeWeight.patchValue("0");
    this.f.diamondsTradeWeight.patchValue("0");
    this.f.goodsTradeSum.patchValue("0");
    this.f.expenseName.patchValue('');
    this.f.expenseSum.patchValue('');
    this.f.auctionAmount.patchValue('0');
    this.username = "";
    this.report = new Report();
  }

  valuechange(): void {
    this.loadingReport = true;
    this.reportService.getReportByDateAndBranch(this.f.date.value, this.f.branch.value).subscribe(rpt => {
      if (rpt.length > 0) {
        this.loadedReport = rpt[0];
        this.showReport = true;
        console.log(this.loadedReport);
        this.statsService.getTotalPercent(this.loadedReport._id).subscribe(d => {
          console.log("totalPercentage: " + d);
          this.f.totalPercentRecieved.patchValue(this.transformInput(d));
        });
        this.f.id.patchValue(this.loadedReport._id);
        this.f.loanersPawned.patchValue(this.transformInput(this.loadedReport.loanersPawned.toString()));
        this.f.loanersBought.patchValue(this.transformInput(this.loadedReport.loanersBought.toString()));

        this.f.loanedRub.patchValue(this.transformInput(this.loadedReport.loanedRub.toString()));
        this.f.repayedRub.patchValue(this.transformInput(this.loadedReport.repayedRub.toString()));
        this.f.percentRecieved.patchValue(this.transformInput(this.loadedReport.percentRecieved.toString()));
        // Золото
        this.f.goldBought.patchValue(this.transformInput(this.loadedReport.goldBought.toString()));
        this.f.goldSold.patchValue(this.transformInput(this.loadedReport.goldSold.toString()));
        // Серебро
        this.f.silverBought.patchValue(this.transformInput(this.loadedReport.silverBought.toString()));
        this.f.silverSold.patchValue(this.transformInput(this.loadedReport.silverSold.toString()));
        // Бриллианты
        this.f.diamondBought.patchValue(this.transformInput(this.loadedReport.diamondBought.toString()));
        this.f.diamondSold.patchValue(this.transformInput(this.loadedReport.diamondSold.toString()));
        // Вещи
        this.f.goodsBought.patchValue(this.transformInput(this.loadedReport.goodsBought.toString()));
        this.f.goodsSold.patchValue(this.transformInput(this.loadedReport.goodsSold.toString()));
        // Касса
        this.f.cashboxEvening.patchValue(this.transformInput(this.loadedReport.cashboxEvening.toString()));
        // Торги
        this.f.tradesActive.patchValue(this.transformInput(this.loadedReport.tradesActive.toString()));
        this.f.goldTradeSum.patchValue(this.transformInput(this.loadedReport.goldTradeSum.toString()));
        this.f.goldTradeWeight.patchValue(this.transformInput(this.loadedReport.goldTradeWeight.toString()));
        this.f.silverTradeSum.patchValue(this.transformInput(this.loadedReport.silverTradeSum.toString()));
        this.f.silverTradeWeight.patchValue(this.transformInput(this.loadedReport.silverTradeWeight.toString()));
        this.f.diamondsTradeWeight.patchValue(this.transformInput(this.loadedReport.diamondsTradeWeight.toString()));
        this.f.goodsTradeSum.patchValue(this.transformInput(this.loadedReport.goodsTradeSum.toString()));
        this.f.expenseName.patchValue('');
        this.f.expenseSum.patchValue('');
        if (isUndefined(this.loadedReport.auctionAmount)) {
          this.f.auctionAmount.patchValue(0);
        } else {
          this.f.auctionAmount.patchValue(this.transformInput(this.loadedReport.auctionAmount.toString()));
        }
        this.report = new Report();
        this.report.expenses = this.loadedReport.expenses;
        this.version = this.loadedReport.version;
        this.userService.getUserById(this.loadedReport.user).subscribe(users => this.username = users[0].name);
        this.changeDynamics();
        this.cd.detectChanges();
      } else {
        this.resetForm();
        this.showReport = false;
      }
    }, error => {
      console.log(error);
    });
    this.loadingReport = false;
    this.loadingDynamics = true;
    this.dynamicsService.getDynamics(this.f.branch.value, this.f.date.value).subscribe(dyn => {
      this.dynamics = dyn;
      this.changeDynamics();
      this.loadingDynamics = false;
    });

  }

  saveReport(): void {
    this.reportService.create(
      this.loadedReport._id,
      this.loadedReport.branch,
      new Date(this.loadedReport.date).toString(),
      this.loadedReport.loanersPawned.toString(),
      this.loadedReport.loanersBought.toString(),
      this.loadedReport.loanedRub.toString(),
      this.loadedReport.repayedRub.toString(),
      this.loadedReport.percentRecieved.toString(),
      this.loadedReport.goldBought.toString(),
      this.loadedReport.goldSold.toString(),
      this.loadedReport.silverBought.toString(),
      this.loadedReport.silverSold.toString(),
      this.loadedReport.diamondBought.toString(),
      this.loadedReport.diamondSold.toString(),
      this.loadedReport.goodsBought.toString(),
      this.loadedReport.goodsSold.toString(),
      this.loadedReport.cashboxEvening.toString(),
      this.loadedReport.tradesActive.toString(),
      this.loadedReport.goldTradeSum.toString(),
      this.loadedReport.goldTradeWeight.toString(),
      this.loadedReport.silverTradeSum.toString(),
      this.loadedReport.silverTradeWeight.toString(),
      this.loadedReport.diamondsTradeWeight.toString(),
      this.loadedReport.goodsTradeSum.toString(),
      this.loadedReport.expenses,
      this.getValue(this.f.auctionAmount.value),
      this.version
    ).subscribe(data => {
      console.log(data);
      this.notificationDialog.open("Спасибо, отчет принят", true);
    }, error => {
      console.log(error);
      this.notificationDialog.open("Спасибо, отчет принят", true);
    });
  }

  goPrev(): void {
    let index = this.branches.map(m => {
      return m._id
    }).indexOf(this.f.branch.value);
    console.log(index);
    if (index == -1) {
      return;
    }
    if (index > 0) {
      this.f.branch.patchValue(this.branches[index - 1]._id);
    }
  }

  goNext(): void {
    let index = this.branches.map(m => {
      return m._id
    }).indexOf(this.f.branch.value);
    console.log(index);
    if (index == -1) {
      return;
    }
    if (index < this.branches.length - 1) {
      this.f.branch.patchValue(this.branches[index + 1]._id);
    }
  }

  hasPrev(): boolean {
    let br = this.f.branch.value;
    if (br == "") return false;
    let index = this.branches.map(m => {
      return m._id
    }).indexOf(br);
    if (index == -1) return false;
    return index > 0;
  }

  hasNext(): boolean {
    let br = this.f.branch.value;
    if (br == "") return false;
    let index = this.branches.map(m => {
      return m._id
    }).indexOf(br);
    if (index == -1) return false;
    return index < this.branches.length - 1;
  }

  showArrows(): boolean {
    let user = JSON.parse(localStorage.getItem("currentUser"));
    return user.roles.indexOf("admin") >= 0;
  }

  transformInput(value: string): string {
    return parseFloat(value).toLocaleString("ru-RU");
  }

  public keyDown(event, allowDecimals: boolean) {
    Tools.allowNumbers(event, allowDecimals);
  }

  public keyUp(event) {
    event.target.value = Tools.formatNum(event.target.value);
  }

  onBlurFixed(event) {
    event.target.value = Tools.strToNum(event.target.value).toLocaleString("ru-RU");
  }

  getValue(value: string): string {
    if (value == "") {
      return "0";
    }
    return value.replace(/\s/g, '').replace(/,/g, '.');
  }
}



