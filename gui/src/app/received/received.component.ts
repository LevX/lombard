import {Component, OnInit} from '@angular/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Branch} from "../_models";
import {Report} from "../_models/report";
import {Observable, timer} from 'rxjs';
import {BranchService} from "../_services/branch.service";
import {UserService} from "../_services/user.service";
import {LegalParty} from "../_models/legalParty";
import {LegalPartyService} from "../_services/legalParty_service";
import {ReportService} from "../_services/report.service";
import {DateFormats} from "../_helpers/dateFormats";
import {TreasuryInfo} from "../_models/treasuryInfo";

@Component({
  selector: 'app-received',
  templateUrl: './received.component.html',
  styleUrls: ['./received.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DateFormats},
  ]
})
export class ReceivedComponent implements OnInit {

  receivedForm: FormGroup = new FormGroup({
    receivedDate: new FormControl()
  });

  receivedLegalForm: FormGroup = new FormGroup({
    legalReceiveDate: new FormControl()
  });

  private reports: Report[] = [];
  private branches: Branch[] = [];
  missingBranches: Branch[] = [];
  private legalParties: LegalParty[] = [];
  private treasury: TreasuryInfo[] = [];
  missingLegalParties: LegalParty[] = [];

  public totalBranchCount = 0;
  public receivedBranchCount = 0;
  public totalLegalPartiesCount = 0;
  public receivedLegalPartiesCount = 0;


  tmr: Observable<number> = timer(0, 5 * 60 * 1000);

  get received() {
    return this.receivedForm.controls;
  }

  get legal() {
    return this.receivedLegalForm.controls;
  }

  constructor(private userService: UserService,
              private branchService: BranchService,
              private reportService: ReportService,
              private legalPartyService: LegalPartyService) {
    let now = new Date();
    if (now.getHours() < 8) {
      now = new Date(new Date().getTime() - 60 * 60 * 24 * 1000);
    }

    now.setHours(0, 0, 0, 0);
    this.received.receivedDate.patchValue(now);
    this.legal.legalReceiveDate.patchValue(now);
    this.branchService.getAll().subscribe(b => {
      this.branches = b;
      this.totalBranchCount = this.branches.length;
      this.branchDataChanged();
    });
    this.legalPartyService.getAll().subscribe(b => {
      this.legalParties = b;
      this.totalLegalPartiesCount = this.legalParties.length;
      this.legalPartyDataChange();
    });
    this.branchDataChanged();
    this.legalPartyDataChange();
    this.receivedForm.valueChanges.subscribe(result => this.branchDataChanged());
    this.receivedLegalForm.valueChanges.subscribe(result => this.legalPartyDataChange());
    this.tmr.subscribe(() => {
      this.branchDataChanged();
      this.legalPartyDataChange();
    });
  }

  legalPartyDataChange() {
    this.legalPartyService.getBankDataForDate(this.legal.legalReceiveDate.value).subscribe(r => {
      this.treasury = r;
      console.log(r);
      let receivedLegalParties = this.treasury.map(function (r) {
        return r.legalPartyId;
      });
      console.log(receivedLegalParties);
      this.receivedLegalPartiesCount = receivedLegalParties.length;
      this.missingLegalParties = [];
      for (let i = 0; i < this.legalParties.length; i++) {
        if (receivedLegalParties.indexOf(this.legalParties[i]._id) == -1) {
          this.missingLegalParties.push(this.legalParties[i])
        }
      }
    });
  }

  branchDataChanged() {
    this.reportService.getReportsByDate(this.received.receivedDate.value).subscribe(r => {
      this.reports = r;
      let receivedBranches = this.reports.map(function (r) {
        return r.branch
      });
      this.receivedBranchCount = receivedBranches.length;
      this.missingBranches = [];
      for (let i = 0; i < this.branches.length; i++) {
        if (receivedBranches.indexOf(this.branches[i]._id) == -1) {
          this.missingBranches.push(this.branches[i])
        }
      }
    });
  }

  ngOnInit() {
  }

}
