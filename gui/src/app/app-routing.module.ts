import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NewReportComponent} from './new-report/new-report.component';
import {UsersComponent} from "./users/users.component";
import {BranchesComponent} from "./branches/branches.component";
import {ViewReportComponent} from "./view-report/view-report.component";
import {StatsComponent} from "./stats/stats.component";
import {ExcelReportComponent} from './excel-report/excel-report.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './_helpers/auth.guard';
import {CatalogComponent} from "./catalog/catalog.component";


const routes: Routes = [
  {path: '', component: NewReportComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'newReport', component: NewReportComponent},
  {path: 'viewReport', component: ViewReportComponent},
  {path: 'stats', component: StatsComponent},
  {path: 'users', component: UsersComponent},
  {path: 'branches', component: BranchesComponent},
  {path: 'excel', component: ExcelReportComponent},
  {path: 'catalog', component: CatalogComponent},
  {path: '**', redirectTo: ''}
];

export const appRoutingModule = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
